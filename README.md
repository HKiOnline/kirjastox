# Kirjasto X - sovellus

Kirjasto X sovellus on Java FXML:llä tehty kirjastonhoitajan työväline asiakkaiden, teosten ja lainojen hallintaan.
Sovellus ei vielä sellaisenaan sovellu kunnallisen kirjaston käyttöön vaan on tarkoitettu ennemminkin proto/demo-sovellukseksi.

Kirjasto X on tehty ryhmätyönä. Sen [lähdenkoodin löydät Bitbucketista](http://vcs.hkionline.net/kirjastox). [Commit-sivulta näet contribuuttorit](http://vcs.hkionline.net/kirjastox/commits/all).

## Vaatimukset sovelluksen suorittamiseen

- [Java 7](https://java.com/getjava) 
- [Lähdekoodin](http://vcs.hkionline.net/kirjastox/src/) tai suoritettavan JAR-tiedoston (tulossa).

## Vaatimukset sovelluksen kehittämiseen

- Java 7 JDK
- Java FX 2.2
- [Netbeans IDE 7.4](https://netbeans.org/downloads/)
- [Scene Builder 1.1](http://www.oracle.com/technetwork/java/javafx/downloads/index-jsp-136193.html)
- [Git](http://git-scm.com)

Voit kloonata tämän varaston itsellesi komennolla "git clone https://HKiOnline@bitbucket.org/HKiOnline/kirjastoX.git Kirjasto" terminaalista tai käyttää apunasi sovellusta kuten GitGUI, GitHub tai SourceTree ja antaa edellä olevast komennosta URL:in koodivarastoon.

1. Kloonaa varasto: git clone https://HKiOnline@bitbucket.org/HKiOnline/kirjastoX.git KirjastoX
1. Avaa projekti Netbeansissä: File > Open Project.
1. Valitse Kirjasto-projektin hierarkisesta projekti-näkymästä Source Packages > kirjasto
1. Suorita Clean and Build komento päävalikosta Run > Clean and Build. Varmista, että projektin tiedostot kääntyivät eikä virheitä tullut.
1. Jos Clean and Build tapahtui ilman virheitä valitse päävalikosta Run > Run Project (KirjastoX)


