/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package kirjastox.model;

import java.io.Serializable;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author hki
 */
public class Title implements Serializable{

    /*
    private final SimpleStringProperty name;
    private final SimpleStringProperty creator;
    private final SimpleIntegerProperty yearPublished;
    private final SimpleStringProperty ISBN;
    private final SimpleIntegerProperty units;
    private final SimpleIntegerProperty unitsAvailable;
*/
    private String name;
    private String creator;
    private int yearPublished;
    private String ISBN;
    private int units;
    private int unitsAvailable;
    
    protected Title(String name, String creator, int yearPublished, String ISBN, int units){
        /*
        this.name = new SimpleStringProperty(name);
        this.creator = new SimpleStringProperty(creator);
        this.yearPublished = new SimpleIntegerProperty(yearPublished);
        this.ISBN = new SimpleStringProperty(ISBN);
        this.units = new SimpleIntegerProperty(units);
        this.unitsAvailable = new SimpleIntegerProperty(units);
        */
        
        this.name = name;
        this.creator = creator;
        this.yearPublished = yearPublished;
        this.ISBN = ISBN;
        this.units = units;
        this.unitsAvailable = units;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the creator
     */
    public String getCreator() {
        return creator;
    }

    /**
     * @param creator the creator to set
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * @return the yearPublished
     */
    public int getYearPublished() {
        return yearPublished;
    }

    /**
     * @param yearPublished the yearPublished to set
     */
    public void setYearPublished(int yearPublished) {
        this.yearPublished = yearPublished;
    }

    /**
     * @return the ISBN
     */
    public String getISBN() {
        return ISBN;
    }

    /**
     * @param ISBN the ISBN to set
     */
    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }

    /**
     * @return the units
     */
    public int getUnits() {
        return units;
    }

    /**
     * @param units the units to set
     */
    public void setUnits(int units) {
        this.units = units;
    }

    /**
     * @return the unitsAvailable
     */
    public int getUnitsAvailable() {
        return unitsAvailable;
    }

    /**
     * @param unitsAvailable the unitsAvailable to set
     */
    public void setUnitsAvailable(int unitsAvailable) {
        this.unitsAvailable = unitsAvailable;
    }
    
    public void decrementUnitsAvailable() {
        this.unitsAvailable--;
    }
    
    public void incrementUnitsAvailable() {
        this.unitsAvailable++;
    }
    
    public boolean isAvailable(){
        return (this.getUnitsAvailable()>0);
    }
}
