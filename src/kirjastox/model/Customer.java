/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package kirjastox.model;

import kirjastox.model.Book;
import java.io.Serializable;
import java.util.ArrayList;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import kirjastox.model.AudioRecord;


/**
 *
 * @author hki
 */
public class Customer implements Serializable{    

    /*
    private final SimpleStringProperty name; // Asiakkaan nimi
    private final SimpleStringProperty address; // Asiakkaan kotiosoite
    private final SimpleIntegerProperty cardNumber; // Asiakkaan kirjastokortin numero 
    private ArrayList<Book> bookLoans; // Asiakkaan lainaamat kirjat
    private ArrayList<AudioRecord> audioRecords; 
    private final SimpleDoubleProperty fines; 
    private final SimpleBooleanProperty loanBan;

    //Uusin käyttämätön kortin numero
    private static SimpleIntegerProperty nextUnusedCardNumber;
*/
    private String name; // Asiakkaan nimi
    private String address; // Asiakkaan kotiosoite
    private int cardNumber; // Asiakkaan kirjastokortin numero 
    private ArrayList<Book> bookLoans; // Asiakkaan lainaamat kirjat
    private ArrayList<AudioRecord> audioRecords; 
    private double fines; 
    private boolean loanBan;

    /**
     *
     * @param name
     * @param address
     */
    public Customer(String name, String address, int cardNumber){
    /*    
        this.name = new SimpleStringProperty(name);
        this.address = new SimpleStringProperty(address);
        
        int firstCard = 1;
        this.nextUnusedCardNumber = new SimpleIntegerProperty(firstCard);
        
        this.cardNumber = new SimpleIntegerProperty(firstCard);
        int cardIntNumber = this.getCardNumber();
        cardIntNumber++;
        this.nextUnusedCardNumber.set(cardIntNumber);
        
        this.bookLoans = new ArrayList<>();
        this.audioRecords = new ArrayList<>();
        
        this.fines = new SimpleDoubleProperty(0.0);
        this.loanBan = new SimpleBooleanProperty(false);
    */
        this.name = name;
        this.address = address;
        this.cardNumber = cardNumber;
        this.bookLoans = new ArrayList<>();
        this.audioRecords = new ArrayList<>();
        this.fines = 0.0;
        this.loanBan = false;
    }
    
    public void setStandardFine() {
        double oldFines = this.getFines();
        this.setFines(oldFines+0.5);
        if(this.getFines() > 5.0){
            this.setLoanBan(true);
        }
    }

    /**
     * @return the loanBan
     */
    
    public boolean hasLoanBan() {
        return loanBan;
    }

    /**
     * @param loanBan the loanBan to set
     */
    public void setLoanBan(boolean loanBan) {
        this.loanBan = loanBan;
    }
    
    public void payFines(){
        this.setFines(0.0);
        if(this.hasLoanBan()){
            this.setLoanBan(false);
        }
    }
   
    
    public void loanBook(Book title){
        if(title.isAvailable() && !this.hasLoanBan()){
            this.getBookLoans().add(title);
        }
    }

    public void returnBook(Book title){
        for (int i = 0; i < this.getBookLoans().size(); i++){
	 		if (this.getBookLoans().get(i).equals(title)){ 
	 			this.getBookLoans().remove(i);
	 		}
	 	}
    }
    
    public void buyAudioRecord(AudioRecord title){
        if(title.isAvailable() && !this.hasLoanBan()){
            this.getAudioRecords().add(title);
        }
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the cardNumber
     */
    public int getCardNumber() {
        return cardNumber;
    }

    /**
     * @param cardNumber the cardNumber to set
     */
    public void setCardNumber(int cardNumber) {
        this.cardNumber = cardNumber;
    }

    /**
     * @return the bookLoans
     */
    public ArrayList<Book> getBookLoans() {
        return bookLoans;
    }

    /**
     * @param bookLoans the bookLoans to set
     */
    public void setBookLoans(ArrayList<Book> bookLoans) {
        this.bookLoans = bookLoans;
    }

    /**
     * @return the audioRecords
     */
    public ArrayList<AudioRecord> getAudioRecords() {
        return audioRecords;
    }

    /**
     * @param audioRecords the audioRecords to set
     */
    public void setAudioRecords(ArrayList<AudioRecord> audioRecords) {
        this.audioRecords = audioRecords;
    }

    /**
     * @return the fines
     */
    public double getFines() {
        return fines;
    }

    /**
     * @param fines the fines to set
     */
    public void setFines(double fines) {
        this.fines = fines;
    }

    /**
     * @return the loanBan
     */
    public boolean isLoanBan() {
        return loanBan;
    }    
}
