/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package kirjastox.model;

import java.io.Serializable;
import kirjastox.model.Title;


/**
 *
 * @author hki
 */
public class Book extends Title implements Serializable{
    
    public Book(String name, String creator, int yearPublished, String ISBN, int units){
        super(name, creator, yearPublished, ISBN, units);
    }
    
    
    
    public void setUnits(int nidelkm) {
        // Jos täältä tulee virhe, tarkista alustukset
        // jos niteitä on enemmän kuin ennen niin lisää myös vapaina olevien niteiden määrää uuden ja vanhan erotuksella
        int vanhaNideLkm = this.getUnits();
        int erotus = nidelkm - vanhaNideLkm;
        int vapaana = this.getUnitsAvailable();
        int niteitaVapaana = (vapaana+erotus);
        
        // Asetataan uusien niteiden määrä tässä, ennen kuin päivitetään vapaat niteet
        // Järjestyksellä on väliä tarkistusten kannalta
        super.setUnits(nidelkm);
        
        // jos niteitä on vähemmän kuin ennen, niin vähennä vapaina olevien niteiden määrää nollaan saakka, muttei negatiiviseksi
            // tämä tarkistus hoidetaan setVapaidenNiteidenLkm() metodissa
        // entäpä jos mennään negatiiviseen ja niteitä on lainassa enemmän kuin niitä on olemassa?
        super.setUnitsAvailable(niteitaVapaana);
    }
    
    public void setAvailableUnits(int niteet) {
        // Tarkista, että vapaana on minimissään nolla ja maksimissaan niteiden lukumäärä
        // Tarkistetaan, ettei vapaita niteitä voi olla enemmän kuin olemassaolevia niteitä
        if(niteet > this.getUnits()){
            niteet = this.getUnits();
        }
        // Jos vapaita niteitä on alle nolla, pakotetaan vapaiden niteiden määrä nollaksi 
        if(niteet < 0){
            niteet = 0;
        }
        
        // Aseta vapaiden niteiden uusi määrä
        super.setUnitsAvailable(niteet);
    }
}
