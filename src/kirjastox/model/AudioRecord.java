/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package kirjastox.model;

import kirjastox.model.Title;
import java.io.Serializable;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author hki
 */
public class AudioRecord extends Title implements Serializable{
    /*
    private SimpleIntegerProperty tracks;
    private SimpleDoubleProperty price;
    private SimpleStringProperty recordCompany;
*/
    private int tracks;
    private double price;
    private String recordCompany;
    
    public AudioRecord(String name, String creator, int yearPublished, String ISBN, int units, int tracks, double price, String recordCompany){
        super(name, creator, yearPublished, ISBN, units);
        /*
        this.tracks = new SimpleIntegerProperty(tracks);
        this.price = new SimpleDoubleProperty(price);
        this.recordCompany = new SimpleStringProperty(recordCompany);
        */
        this.tracks = tracks;
        this.price = price;
        this.recordCompany = recordCompany;
    }
    
    /* Tämä metodi on on ylikuormitettu äitiluokan saman nimisestä metodista.
        Syy ylikuormitukseen on se, että levyjä ei lainata, niitä vain ostetaan ja 
        siksi vapaiden niteiden määrä vaikuttaa suoraan myös olemassa olevien niteiden määrään
    */
    
     public void setAvailableUnits(int vapaidenNiteidenLkm){         
         super.setUnits(vapaidenNiteidenLkm);
         super.setUnitsAvailable(vapaidenNiteidenLkm);
     }
     
     public void setUnits(int nidelkm) {
        super.setUnits(nidelkm);
        super.setUnitsAvailable(nidelkm);
    }
     
     public void decrementUnitsAvailable() {
        int units = this.getUnits();
        units--;
        this.setAvailableUnits(units);
    }

    /**
     * @return the tracks
     */
    public int getTracks() {
        return tracks;
    }

    /**
     * @param tracks the tracks to set
     */
    public void setTracks(int tracks) {
        this.tracks = tracks;
    }

    /**
     * @return the price
     */
    public double getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /**
     * @return the recordCompany
     */
    public String getRecordCompany() {
        return recordCompany;
    }

    /**
     * @param recordCompany the recordCompany to set
     */
    public void setRecordCompany(String recordCompany) {
        this.recordCompany = recordCompany;
    }
}
