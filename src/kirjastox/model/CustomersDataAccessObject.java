/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package kirjastox.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 *
 * @author hki
 */
public class CustomersDataAccessObject {
    
    private ArrayList<Customer> customers;
    
    public CustomersDataAccessObject(){
        readSerializedData();
    }
    
    public ArrayList<Customer> getAll(){
        return customers;
    }
    
    public void add(Customer customer){
        customers.add(customer);
    }
    
    public void remove(Customer customer){
        customers.remove(customer);
    }
    
    public void updateAll(ArrayList<Customer> customers){
        this.customers = customers;
    }

    public void save(){
        // Tätä täytyy kutsua, jos mielii, että data säilyy seuraavaankin ohjelman avaamiseen
        serializeData();
    }
    
    public void setBookLoanToCustomer(Book loanBook, Customer loaner){
        Customer customer = null; 
        for (int i = 0; i < customers.size(); i++){
            if (customers.get(i).equals(loaner)){
                customer = customers.get(i);
                customer.loanBook(loanBook);
            }
        }
    }
    
    //asettaa lainatun kirjan asiakkaan loans-listaan 
     public void setBookLoanToCustomer(Book loanBook, int cardNum){
        Customer customer = null; 
        for (int i = 0; i < customers.size(); i++){
            if (customers.get(i).getCardNumber() == cardNum){
                customer = customers.get(i);
                customer.loanBook(loanBook);
            }
        }
    }
     
   //palauttaa asiakkaan kirjastokortin numeron perusteella
    public Customer getCustomerByCardNumber(int cardNum){
        Customer customer = null; 
        for (int i = 0; i < customers.size(); i++){
            if (customers.get(i).getCardNumber() == cardNum){
                customer = customers.get(i);
            }
        }
         return customer;
    }
    
    public int getCustomerIndexByCardNumber(int cardNum){
        Customer customer = null;
        int customerIndex = -1;
        for (int i = 0; i < customers.size(); i++){
            if (customers.get(i).getCardNumber() == cardNum){
                customerIndex = i;
            }
        }

       return customerIndex;
    }
       
        
    public void setAudioRecordPurchaseToCustomer(AudioRecord newRecord, Customer owner){
      Customer customer = null; 
        for (int i = 0; i < customers.size(); i++){
            if (customers.get(i).equals(owner)){
                customer = customers.get(i);
                customer.buyAudioRecord(newRecord);
            }
        }
    }
    
    private void readSerializedData(){
        // lue sarjallistettu tiedosto asiakkaista
       // Valmistellaan streamit tiedoston ja objektien lukua varten
        FileInputStream fileIn = null;
        ObjectInputStream in = null;
        File dataFile = new File("customers.data");
        
        try {
            // Yritetään avata tiedosto ja avata objekti-streami
            fileIn = new FileInputStream(dataFile);
            in = new ObjectInputStream(fileIn);
            // Yritetään lukea kirja-objektit listaan
            customers = (ArrayList<Customer>) in.readObject();
        } catch (Exception ex) {
            // Tähän poikkeukseen törmätään yleensä, jos meillä ei ole olemassa levyjen data-lähdettä. Luodaan sellainen.
            System.out.println("AudioRecordDataAccessObject: Serialized file for books could not be read. Creating a new data-file with dummy data. You may wish to restart the application to see the generated data.");
            
            // generoidaan dataa testimielessä
            customers = this.generateDummyData();
            
            // serialisoidaan data tiedostoon seuraavaa avauskertaa varten
            this.serializeData();

        } finally {
            if (in != null) {
                try {
                    in.close();
                    fileIn.close();
                } catch (IOException ie) {
                    ie.printStackTrace();
                }
            }
        }
    }
    
    private void serializeData(){
        // sarjallista kaikki asiakkaat
        
        ObjectOutputStream out = null;
        FileOutputStream fileOut = null;
        
        try {
            
            File dataFile = new File("customers.data");
            try {
                // Luo uuden tiedoston, jos ja vain jos tiedostoa ei ole jo olemassa
                dataFile.createNewFile();
                fileOut = new FileOutputStream(dataFile);
                out = new ObjectOutputStream(fileOut);
                out.writeObject(customers);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            
            
            
        } catch (Exception ex) {
            System.out.println("CustomersDataAccessObject: Serialized file for books could not be written due to an error in serialization.");
        } finally {
            if (out != null) {
                try {
                    out.close();
                    fileOut.close();
                } catch (IOException ie) {
                    ie.printStackTrace();
                }
            }
        }
    }
    
    private ArrayList<Customer> generateDummyData(){
        //String name, String creator, int yearPublished, String ISBN, int units, int tracks, double price, String recordCompany
        
        ArrayList<Customer> dummyCustomers = new ArrayList<>();
        
        dummyCustomers.add(new Customer("Late Lainaaja", "Lainakuja 7", 1));
        dummyCustomers.add(new Customer("Pertti Testimies", "Testauskatu 28", 2));
        dummyCustomers.add(new Customer("Maija Musiikko", "Nuottikatu 18", 3));
        dummyCustomers.add(new Customer("Matti Musiikko", "Nuottikatu 18", 4));
        dummyCustomers.add(new Customer("Jaana Jammailija", "Sellokatu 18", 5));

    
        return dummyCustomers;
    }
    
}
