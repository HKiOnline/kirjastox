/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package kirjastox.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;


/**
 *
 * @author hki
 */
public class BooksDataAccessObject implements Serializable{
    
    private ArrayList<Book> books;
    
    public BooksDataAccessObject(){
        readSerializedData();
    }
    
    public BooksDataAccessObject(Book book){
    }
    
    public ArrayList<Book> getAll(){
        return books;
    }
    
    public void add(Book book){
        books.add(book);
    }
    
    public void remove(Book book){
        books.remove(book);
    }
    
    public void updateAll(ArrayList<Book> books){
        this.books = books;
    }
    
    public void save(){
        serializeData();
    }
        
    private void readSerializedData(){
        // Valmistellaan streamit tiedoston ja objektien lukua varten
        FileInputStream fileIn = null;
        ObjectInputStream in = null;
        File dataFile = new File("books.data");
        
        try {
            // Yritetään avata tiedosto ja avata objekti-streami
            fileIn = new FileInputStream(dataFile);
            in = new ObjectInputStream(fileIn);
            // Yritetään lukea kirja-objektit listaan
            books = (ArrayList<Book>) in.readObject();
        } catch (Exception ex) {
            // Tähän poikkeukseen törmätään yleensä, jos meillä ei ole olemassa kirjojen data-lähdettä. Luodaan sellainen.
            System.out.println("BooksDataAccessObject: Serialized file for books could not be read. Creating a new data-file with dummy data. You may wish to restart the application to see the generated data.");
            
            // generoidaan dataa testimielessä
            books = this.generateDummyData();
            
            // serialisoidaan data tiedostoon seuraavaa avauskertaa varten
            this.serializeData();

        } finally {
            if (in != null) {
                try {
                    in.close();
                    fileIn.close();
                } catch (IOException ie) {
                    ie.printStackTrace();
                }
            }
        }
   }
    
    private void serializeData(){
        ObjectOutputStream out = null;
        FileOutputStream fileOut = null;
        
        try {
            
            File dataFile = new File("books.data");
            try {
                // Luo uuden tiedoston, jos ja vain jos tiedostoa ei ole jo olemassa
                dataFile.createNewFile();
                fileOut = new FileOutputStream(dataFile);
                out = new ObjectOutputStream(fileOut);
                out.writeObject(books);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            
            
            
        } catch (Exception ex) {
            System.out.println("BooksDataAccessObject: Serialized file for books could not be written due to an error in serialization.");
        } finally {
            if (out != null) {
                try {
                    out.close();
                    fileOut.close();
                } catch (IOException ie) {
                    ie.printStackTrace();
                }
            }
        }
    }
    
    private ArrayList<Book> generateDummyData(){
        ArrayList<Book> dummyBooks = new ArrayList<>();
        dummyBooks.add(new Book("Genroitu 1", "Dao Orm", 2009, "gen-ero-itu-1", 5));
        dummyBooks.add(new Book("Genroitu 2", "Dao Orm", 2010, "gen-ero-itu-2", 10));
        dummyBooks.add(new Book("Genroitu 3", "Dao Orm", 2011, "gen-ero-itu-3", 15));
        dummyBooks.add(new Book("Genroitu 4", "Dao Orm", 2012, "gen-ero-itu-4", 20));
        dummyBooks.add(new Book("Genroitu 5", "Dao Orm", 2013, "gen-ero-itu-5", 25));
    
        return dummyBooks;
    }
}
