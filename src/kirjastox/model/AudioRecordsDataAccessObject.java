/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package kirjastox.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author hki
 */
public class AudioRecordsDataAccessObject implements Serializable{
    
    private ArrayList<AudioRecord> audioRecords;
    
    public AudioRecordsDataAccessObject(){
        readSerializedData();
    }
    
    public AudioRecordsDataAccessObject(AudioRecord record){
    }
    
    public ArrayList<AudioRecord> getAll(){
        return audioRecords;
    }
    
    public void add(AudioRecord record){
        audioRecords.add(record);
    }
    
    public void remove(AudioRecord record){
        audioRecords.remove(record);
    }
    
    public void updateAll(ArrayList<AudioRecord> records){
        this.audioRecords = records;
    }
    
    public void save(){
        serializeData();
    }
        
    private void readSerializedData(){
        // Valmistellaan streamit tiedoston ja objektien lukua varten
        FileInputStream fileIn = null;
        ObjectInputStream in = null;
        File dataFile = new File("records.data");
        
        try {
            // Yritetään avata tiedosto ja avata objekti-streami
            fileIn = new FileInputStream(dataFile);
            in = new ObjectInputStream(fileIn);
            // Yritetään lukea kirja-objektit listaan
            audioRecords = (ArrayList<AudioRecord>) in.readObject();
        } catch (Exception ex) {
            // Tähän poikkeukseen törmätään yleensä, jos meillä ei ole olemassa levyjen data-lähdettä. Luodaan sellainen.
            System.out.println("AudioRecordDataAccessObject: Serialized file for books could not be read. Creating a new data-file with dummy data. You may wish to restart the application to see the generated data.");
            
            // generoidaan dataa testimielessä
            audioRecords = this.generateDummyData();
            
            // serialisoidaan data tiedostoon seuraavaa avauskertaa varten
            this.serializeData();

        } finally {
            if (in != null) {
                try {
                    in.close();
                    fileIn.close();
                } catch (IOException ie) {
                    ie.printStackTrace();
                }
            }
        }
   }
    
    private void serializeData(){
        ObjectOutputStream out = null;
        FileOutputStream fileOut = null;
        
        try {
            
            File dataFile = new File("records.data");
            try {
                // Luo uuden tiedoston, jos ja vain jos tiedostoa ei ole jo olemassa
                dataFile.createNewFile();
                fileOut = new FileOutputStream(dataFile);
                out = new ObjectOutputStream(fileOut);
                out.writeObject(audioRecords);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            
            
            
        } catch (Exception ex) {
            System.out.println("AudioRecordDataAccessObject: Serialized file for books could not be written due to an error in serialization.");
        } finally {
            if (out != null) {
                try {
                    out.close();
                    fileOut.close();
                } catch (IOException ie) {
                    ie.printStackTrace();
                }
            }
        }
    }
    
    private ArrayList<AudioRecord> generateDummyData(){
        //String name, String creator, int yearPublished, String ISBN, int units, int tracks, double price, String recordCompany
        
        ArrayList<AudioRecord> dummyAudioRecords = new ArrayList<>();
        dummyAudioRecords.add(new AudioRecord("Generock Hits 1", "Gene Roy", 2009, "gen-ero-itu-1", 5, 25, 25.0, "Genric Records"));
        dummyAudioRecords.add(new AudioRecord("Generock Hits 2", "Gene Roy", 2010, "gen-ero-itu-2", 10, 20, 21.0, "Genric Records"));
        dummyAudioRecords.add(new AudioRecord("Generock Hits 3", "Gene Roy", 2011, "gen-ero-itu-3", 15, 15, 16.0, "Genric Records"));
        dummyAudioRecords.add(new AudioRecord("Generock Hits 4", "Gene Roy", 2012, "gen-ero-itu-4", 20, 10, 14.0, "Genric Records"));
        dummyAudioRecords.add(new AudioRecord("Generock Hits 5", "Gene Roy", 2013, "gen-ero-itu-5", 25, 5, 9.0, "Genric Records"));
    
        return dummyAudioRecords;
    }
}

