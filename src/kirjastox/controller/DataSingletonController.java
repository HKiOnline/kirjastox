/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package kirjastox.controller;

import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import kirjastox.model.AudioRecord;
import kirjastox.model.AudioRecordsDataAccessObject;
import kirjastox.model.Book;
import kirjastox.model.BooksDataAccessObject;
import kirjastox.model.Customer;
import kirjastox.model.CustomersDataAccessObject;

/**
 *
 * @author hki
 */
public class DataSingletonController {
                
    private String appInfoMessage;
    private ObservableList<Book> books;
    private ObservableList<AudioRecord> audioRecords;
    private ObservableList<Customer> customers;

    // Private constructor prevents instantiation from other classes
    private DataSingletonController(){
        this.appInfoMessage ="Tervetuloa Kirjasto-sovellukseen.";

        BooksDataAccessObject booksPersistentData = new BooksDataAccessObject();            
        books = FXCollections.observableArrayList(booksPersistentData.getAll());
        
        AudioRecordsDataAccessObject audioRecordsPersistentData = new AudioRecordsDataAccessObject();
        audioRecords = FXCollections.observableArrayList(audioRecordsPersistentData.getAll());
        
        CustomersDataAccessObject customersPersistentData = new CustomersDataAccessObject();
        customers = FXCollections.observableArrayList(customersPersistentData.getAll());
    }

    /**
    * SingletonHolder is loaded on the first execution of Singleton.getInstance() 
    * or the first access to SingletonHolder.INSTANCE, not before.
    */
    private static class SingletonHolder { 
            public static final DataSingletonController INSTANCE = new DataSingletonController();
    }

    public static DataSingletonController getInstance() {
            return SingletonHolder.INSTANCE;
    }

    
    /*
        Apumetodeita ohjaimien väliseen kommunikaatioon ja tiedon tallentamiseen
    */
    
    public void setBookLoanToCustomer(Book loanBook, int cardNumber){
        CustomersDataAccessObject customersPersistentData = new CustomersDataAccessObject();
        
        int index = customersPersistentData.getCustomerIndexByCardNumber(cardNumber);
        this.customers.get(index).loanBook(loanBook);
        
        ArrayList<Customer> updatedCustomers = new ArrayList<Customer>(customers);
        customersPersistentData.updateAll(updatedCustomers);
        customersPersistentData.save();
    }
    
    public void setBookReturned(Book returningBook){
        int bookIndex = this.getBooks().indexOf(returningBook);
        Book bookTitle = this.getBooks().get(bookIndex);
        bookTitle.incrementUnitsAvailable();
        
        this.updateBooksPersistentStore();
    }
    
    public void setAudioRecordToCustomer(AudioRecord recordBought, int cardNumber){
        
        CustomersDataAccessObject customersPersistentData = new CustomersDataAccessObject();
        int index = customersPersistentData.getCustomerIndexByCardNumber(cardNumber);
        
        this.customers.get(index).buyAudioRecord(recordBought);
        
        ArrayList<Customer> updatedCustomers = new ArrayList<Customer>(customers);
        customersPersistentData.updateAll(updatedCustomers);
        customersPersistentData.save();
    }
    
    /* Apumetodit tietovaraston tilan oäivittämiseen, nämä on vain siirretty muista ohjaimista tänne*/
    public void updateBooksPersistentStore(){
        // Tässä päivitetään ja tallennetaan muutokset tiedostoon
        BooksDataAccessObject booksPersistentData = new BooksDataAccessObject();
        ArrayList<Book> updatedBooks = new ArrayList<Book>(books);
        booksPersistentData.updateAll(updatedBooks);
        booksPersistentData.save();
    }
    
    public void updateAudioRecordsPersistentStore(){
        // Tässä päivitetään ja tallennetaan muutokset tiedostoon
        AudioRecordsDataAccessObject audioRecordPersistentData = new AudioRecordsDataAccessObject();
        ArrayList<AudioRecord> updatedAudioRecords = new ArrayList<AudioRecord>(audioRecords);
        audioRecordPersistentData.updateAll(updatedAudioRecords);
        audioRecordPersistentData.save();
    }
    
    public void updateCustomersPersistentStore(){
        // Tässä päivitetään ja tallennetaan muutokset tiedostoon
        CustomersDataAccessObject customersPersistentData = new CustomersDataAccessObject();
        ArrayList<Customer> updatedCustomers = new ArrayList<Customer>(customers);
        customersPersistentData.updateAll(updatedCustomers);
        customersPersistentData.save();
    }
    
    /**
    * @return the test
    */
    public String getAppInfoMessage() {
        return appInfoMessage;
    }

    /**
    * @param message the test to set
    */
    public void setAppInfoMessage(String message) {
        this.appInfoMessage = message;
    }

    /**
    * @return the books
    */
    public ObservableList<Book> getBooks() {
        return books;
    }

    /**
     * @param books the books to set
     */
    public void setBooks(ObservableList<Book> books) {
        this.books = books;
    }

    /**
     * @return the audioRecords
     */
    public ObservableList<AudioRecord> getAudioRecords() {
        return audioRecords;
    }

    /**
     * @param audioRecords the audioRecords to set
     */
    public void setAudioRecords(ObservableList<AudioRecord> audioRecords) {
        this.audioRecords = audioRecords;
    }

    /**
     * @return the customers
     */
    public ObservableList<Customer> getCustomers() {
        return customers;
    }

    /**
     * @param customers the customers to set
     */
    public void setCustomers(ObservableList<Customer> customers) {
        this.customers = customers;
    }
    
}
