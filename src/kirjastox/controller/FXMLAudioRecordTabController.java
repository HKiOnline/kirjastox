/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package kirjastox.controller;

import kirjastox.model.AudioRecord;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import kirjastox.model.AudioRecordsDataAccessObject;

/**
 * FXML Controller class
 *
 * @author hki
 */
public class FXMLAudioRecordTabController implements Initializable {

    
    private ObservableList<AudioRecord> audioRecords;
    private DataSingletonController dataController;
    
    @FXML
    private TableView audioRecordsTableView;
    @FXML
    private Label nameLabel;
    @FXML
    private Label creatorLabel;
    @FXML
    private Label ISBNLabel;
    @FXML
    private Label yearPublishedLabel;
    @FXML
    private Label unitsLabel;
    @FXML
    private Label tracksLabel;
    @FXML
    private Label priceLabel;
    @FXML
    private Label recordCompanyLabel;
    @FXML
    private Label addNameLabel;
    @FXML
    private Label enterCardNumberLabel;
    
    @FXML
    private TextField nameTextField;
    @FXML
    private TextField creatorTextField;
    @FXML
    private TextField ISBNTextField;
    @FXML
    private TextField yearPublishedTextField;
    @FXML
    private TextField unitsTextField;
    @FXML
    private TextField tracksTextField;
    @FXML
    private TextField priceTextField;
    @FXML
    private TextField recordCompanyTextField;
    @FXML
    private TextField cardNumberTextField;
    
    @FXML
    private Button saveRecordButton;
    @FXML
    private Button cancelSaveRecordButton;
    @FXML
    private Button buyingButton;
    @FXML
    private Button buyRecordButton;
    @FXML
    private Button cancelBuyRecordButton;
            
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.dataController = DataSingletonController.getInstance();
        
        //audioRecords = FXCollections.observableArrayList();
        intializeAudioRecordsTableView();
        observeAudioRecordsTableView();
        audioRecordsTableView.getSelectionModel().select(0);
    }
    
    @FXML 
    private void handleBuyingButtonAction(ActionEvent event){
        //showAddRecordUI(false);
        cardNumberTextField.clear();
        showBuyRecordUI(true);
    
    }
    
    @FXML
    private void handleAddAudioRecordButtonAction(ActionEvent event){
        showAddRecordUI(true);
    }
    
    @FXML
    private void handleRemoveAudioRecordButtonAction(ActionEvent event){
        try {
            AudioRecord removableRecord
                    = (AudioRecord) audioRecordsTableView.getSelectionModel()
                    .getSelectedItem();

            this.dataController.getAudioRecords().remove(removableRecord);
            updateAudioRecordsPersistentStore();
        }
        catch (Exception exc) {
            System.out.println("Virhe. Levyä ei poistettu.");
        }
    }
    
    @FXML
    private void handleSaveButtonAction(ActionEvent event) {
        saveRecord();
    }
    
    @FXML
    private void handleCancelSaveRecordButtonAction(ActionEvent event) {
        showAddRecordUI(false);
        clearTextFields();
    }
    
    @FXML
    private void handleBuyRecordButtonAction(ActionEvent event) {
        

        int cardNumber = Integer.parseInt(cardNumberTextField.getText());
        AudioRecord recordBought = (AudioRecord) audioRecordsTableView.getSelectionModel().getSelectedItem();
        
        this.dataController.setAudioRecordToCustomer(recordBought, cardNumber);
        
        showBuyRecordUI(false);
        cardNumberTextField.clear();
        
        System.out.println("Audio Records: audio record bought succesfully");
    }
    
    @FXML
    private void handleCancelBuyRecordButtonAction(ActionEvent event) {
        showBuyRecordUI(false);
        cardNumberTextField.clear();
    }
    
    private void clearTextFields() {
        nameTextField.clear();
        creatorTextField.clear();
        yearPublishedTextField.clear();
        ISBNTextField.clear();
        unitsTextField.clear();
        tracksTextField.clear();
        priceTextField.clear();
        recordCompanyTextField.clear();
    }
    
    private void intializeAudioRecordsTableView() {
    //String name, String creator, int yearPublished, String ISBN, int units, int tracks, double price, String recordCompany
        
        //AudioRecordsDataAccessObject audioRecordsPersistentData = new AudioRecordsDataAccessObject();
        //audioRecords = FXCollections.observableArrayList(audioRecordsPersistentData.getAll());

        TableColumn nameColumn = new TableColumn("Nimi");
        nameColumn.setCellValueFactory(new PropertyValueFactory<AudioRecord, String>("name"));

        TableColumn creatorColumn = new TableColumn("Artisti");
        creatorColumn.setCellValueFactory(new PropertyValueFactory<AudioRecord, String>("creator"));
        
        audioRecordsTableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        audioRecordsTableView.setItems(this.dataController.getAudioRecords());
        audioRecordsTableView.getColumns().addAll(nameColumn, creatorColumn);
    }
    
    private void observeAudioRecordsTableView() {
        audioRecordsTableView.getSelectionModel().selectedItemProperty().addListener(
                new ChangeListener<AudioRecord>() {
                    @Override
                    public void changed(ObservableValue<? extends AudioRecord> ov, AudioRecord oldSelection, AudioRecord newSelection) {

                        //System.out.println(newSelection.getName());
                        
                        if (dataController.getAudioRecords().isEmpty()) {
                            setUILabelContent(null);
                        }
                        else {
                            setUILabelContent(newSelection);
                        }
                    }
                });
    }
    
    private void saveRecord() {
        try {
            AudioRecord newRecord = new AudioRecord(nameTextField.getText(),
                    creatorTextField.getText(),
                    Integer.parseInt(yearPublishedTextField.getText()),
                    ISBNTextField.getText(),
                    Integer.parseInt(unitsTextField.getText()),
                    Integer.parseInt(tracksTextField.getText()),
                    Double.parseDouble(priceTextField.getText()),
                    recordCompanyTextField.getText());
            
            audioRecords.add(newRecord);
            this.dataController.updateAudioRecordsPersistentStore();
        }
        catch (NumberFormatException exc) {
            System.out.println("Tallennus epäonnistui. Tarkasta syötteet.");
        }
        
        showAddRecordUI(false);
        clearTextFields();
        audioRecordsTableView.getSelectionModel().selectLast();
    }
    
    private void setUILabelContent(AudioRecord record) {
        if (record != null) {
            nameLabel.setText(record.getName());
            creatorLabel.setText("Tekijä: " + record.getCreator());
            ISBNLabel.setText("ISBN: " + record.getISBN());
            yearPublishedLabel.setText("Julkaisuvuosi: "
                    + record.getYearPublished());
            unitsLabel.setText("Niteitä: " + record.getUnits());
            tracksLabel.setText("Raitoja: " + record.getTracks());
            priceLabel.setText("Hinta: " + record.getPrice());
            recordCompanyLabel.setText("Levy-yhtiö: "
                    + record.getRecordCompany());
        }
        else {
            nameLabel.setText("");
            creatorLabel.setText("Tekijä:");
            ISBNLabel.setText("ISBN:");
            yearPublishedLabel.setText("Julkaisuvuosi:");
            unitsLabel.setText("Niteitä:");
            tracksLabel.setText("Raitoja:");
            priceLabel.setText("Hinta:");
            recordCompanyLabel.setText("Levy-yhtiö:");
        }
    }
    
    private void showAddRecordUI(boolean show) {
        audioRecordsTableView.setMouseTransparent(show);
        addNameLabel.setVisible(show);
        nameTextField.setVisible(show);
        creatorTextField.setVisible(show);
        ISBNTextField.setVisible(show);
        yearPublishedTextField.setVisible(show);
        unitsTextField.setVisible(show);
        tracksTextField.setVisible(show);
        priceTextField.setVisible(show);
        recordCompanyTextField.setVisible(show);
        saveRecordButton.setVisible(show);
        cancelSaveRecordButton.setVisible(show);
        buyingButton.setVisible(!show);

        setUILabelContent(null);
    }
    
    private void showBuyRecordUI(boolean show) {
        audioRecordsTableView.setMouseTransparent(show);
        enterCardNumberLabel.setVisible(show);
        cardNumberTextField.setVisible(show);
        buyRecordButton.setVisible(show);
        cancelBuyRecordButton.setVisible(show);
        buyingButton.setVisible(!show);
    }
    
    private void updateAudioRecordsPersistentStore() {
        AudioRecordsDataAccessObject recordsPersistentData =
                new AudioRecordsDataAccessObject();
        ArrayList<AudioRecord> newRecords =
                new ArrayList<>(audioRecords);
        recordsPersistentData.updateAll(newRecords);
        recordsPersistentData.save();
    }
}
