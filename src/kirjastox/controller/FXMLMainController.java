/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package kirjastox.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

/**
 * @author henri kesseli
 */
public class FXMLMainController implements Initializable {
    
    DataSingletonController dataController;
    
    @FXML
    private Label userInfoLabel;
    
    @FXML
    private void handleCloseButtonAction(ActionEvent event) {
        System.out.println("Suljetaan sovellus sekunnin kuluttua");
        System.exit(0);
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        // TODO
        this.dataController = DataSingletonController.getInstance();
        getUserInfoLabel().setText(this.dataController.getAppInfoMessage());
    }    

    /**
     * @return the userInfoLabel
     */
    public Label getUserInfoLabel() {
        return userInfoLabel;
    }

    /**
     * @param userInfoLabel the userInfoLabel to set
     */
    public void setUserInfoLabel(Label userInfoLabel) {
        this.userInfoLabel = userInfoLabel;
    }
    
}
