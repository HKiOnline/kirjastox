/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package kirjastox.controller;

import kirjastox.model.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * FXML Controller class
 *
 * @author hki
 */
public class FXMLBooksTabController implements Initializable {

    // Tässä on kaikki ohjaimen tiedonhallinnassa käytetyt jäsen muuttujat
    private ObservableList<Book> books;
    private DataSingletonController dataController;
    
    // Tämä muuttuja kertoo, ollaanko juuri lisäämässä uutta kirjaa
    // Tämä vaikuttaa erityisesti joidenkin UI-elementtien näkyvyystilaan
    private boolean isInAdditionMode;
    
    // Tämä on taulukkonäkymä kaikille kirjoille, ulkoasua muokataan FXML:n kautta
    @FXML
    private TableView booksTableView;
    
    // Tässä on kaikki otsikkokentät, ulkoasua muokataan FXML:n kautta
    @FXML
    private Label nameLabel;
    @FXML
    private Label creatorLabel;
    @FXML
    private Label ISBNLabel;
    @FXML
    private Label yearPublishedLabel;
    @FXML
    private Label unitsLabel;
    @FXML
    private Label unitsAvailableLabel;
    @FXML
    private Label addNameLabel;
    @FXML
    private Label addCardNumberLabel;
    
    // Tässä on kaikki muokattavat tekstikentät, ulkoasua muokataan FXML:n kautta
    @FXML
    private TextField nameTextField;
    @FXML
    private TextField creatorTextField;
    @FXML
    private TextField ISBNTextField;
    @FXML
    private TextField yearPublishedTextField;
    @FXML
    private TextField unitsTextField;
    @FXML
    private TextField cardNumberTextField;
    
    // Tässä on kaikki napit, ulkoasua muokataan FXML:n kautta
    @FXML
    private Button saveBookButton;
    @FXML
    private Button cancelSaveBookButton;
    @FXML
    private Button loanButton;
    @FXML
    private Button cancelLoanButton;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        this.dataController = DataSingletonController.getInstance();
        
        // Are we adding a book and need to hide some field...no!
        isInAdditionMode = false;
        
        // Alustetaan ja täytetään kirjataulukkonäkymä
        intializeBooksTableView();
        
        // Käynnistetään kuuntelija, joka tarkkailee muutoksia taulukkonäkymässä
        observeBookTableView();
        
        // Valitaan ensimmäinen rivi, taulukkonäkymän tarkkailija täyttää yksityiskohtakentät
        booksTableView.getSelectionModel().select(0);
        
    }
        
    @FXML
    private void handleAddBookButtonAction(ActionEvent event){
        // Tämä vain aktivoi kaikki lisäyskentät, ei huolehdi tallennuksesta, sen tekee handleSaveBookButtonAction()
        System.out.println("Books: Add book button pressed, begin adding a new book");
        this.showAddBookUIElements();
    }
    
    @FXML 
    private void handleSaveBookButtonAction(ActionEvent event){
        // Tallennetaan uusi kirja
        //Kirjan kentät: name, creator, yearPublished, ISBN, units
        
        // Haetaan tiedot tekstikentistä
        String name = nameTextField.getText();
        String creator = creatorTextField.getText();
        String ISBN = ISBNTextField.getText();
        int year = Integer.parseInt(yearPublishedTextField.getText());
        int units = Integer.parseInt(unitsTextField.getText());
        
        try{
            // Tallenetaan haetut tiedot uuteen kirja-instanssiin
            Book newBook = new Book(name, creator, year, ISBN, units);
            // Lisätään kirja kokoelmaan
            this.dataController.getBooks().add(newBook);
            // Päivitetään vielä pysyväisvarasto, ts. tiedosto
            this.dataController.updateBooksPersistentStore();
            
            System.out.println("Books: Added a new book with the title "+newBook.getName());
            
        }catch(Exception e){
            System.out.println("Books: Failed to add a new book");
        }
        
        // Pilotetaan lisäyskentät
        this.hideAddBookUIElements();
    }
    
    @FXML 
    private void handleCancelSaveBookButtonAction(ActionEvent event){
        // Perutaan kirjan lisäys, näytetään jälleen normaalit UI-elementit
        this.hideAddBookUIElements();
        booksTableView.getSelectionModel().selectFirst();
    }
    
    private void showAddBookUIElements(){
        // Ensin tyhjennetään tai piilotetaan elementit, joita ei haluta näyttää lisäyksen yhteydessä
        isInAdditionMode = true;
        nameLabel.setText("");
        creatorLabel.setText("Tekijä: ");
        ISBNLabel.setText("ISBN: ");
        yearPublishedLabel.setText("Julkaisuvuosi: ");
        unitsLabel.setText("Niteitä: ");
        unitsAvailableLabel.setVisible(false);
        loanButton.setVisible(false);
        
        // sitten näyttetään elementit, jotka halutaan näyttää lisäyksen yhteydessä
        saveBookButton.setVisible(true);
        cancelSaveBookButton.setVisible(true);
        
        addNameLabel.setVisible(true);
        
        nameTextField.setVisible(true);
        creatorTextField.setVisible(true);
        ISBNTextField.setVisible(true);
        yearPublishedTextField.setVisible(true);
        unitsTextField.setVisible(true);
    }
    
    private void hideAddBookUIElements(){
        // piilotetaan lisäykseen liittyvät elementit
        saveBookButton.setVisible(false);
        cancelSaveBookButton.setVisible(false);
        
        addNameLabel.setVisible(false);
        
        nameTextField.setVisible(false);
        creatorTextField.setVisible(false);
        ISBNTextField.setVisible(false);
        yearPublishedTextField.setVisible(false);
        unitsTextField.setVisible(false);
        
        nameTextField.clear();
        creatorTextField.clear();
        ISBNTextField.clear();
        yearPublishedTextField.clear();
        unitsTextField.clear();
        
        // palautetaan näkyviin normaalisti näkyvät elementit
        isInAdditionMode = false;        
        // Jostain syystä indeksi 0 ei reagoi ennen kuin jokin muu on valittu
        booksTableView.getSelectionModel().selectLast();
        
        
 
        unitsAvailableLabel.setVisible(true);
        loanButton.setVisible(true);
    }
    
    
    
    @FXML
    private void handleRemoveBookButtonAction(ActionEvent event){
        // Tämä toiminto poistaa valitun kirjan listalta ja pysyväisvarastosta
        
        try{
            // Noukitaan valittu kirja
            Book selectedBook = (Book) booksTableView.getSelectionModel().getSelectedItem();
            // Poistetaan se listalta
            this.dataController.getBooks().remove(selectedBook);
            
            // Tallenetaan muutokset myös BooksDataAccessObjectiin, jotta saadaan samat tiedot ensikerrallakin kun sovellus käynnistyy
            this.dataController.updateBooksPersistentStore();
            
            System.out.println("Books: Removed book "+selectedBook.getName());
        }
        catch(Exception e){
            System.out.println("Postossa tapahtui virhe. Todennäköisesti poistettavaa teosta ei valittu");
        }
        
    }
    
    @FXML 
    private void handleLoanButtonAction(ActionEvent event){
        // Tämä hoitaa kirjan lainaustoiminnon
        
        System.out.println("Books: Loaning procedure for selected book has begun");
        
        // Tarkastetaan joko tekstikenttä kirjastokortin numerolle on näkyvissä
        if(addCardNumberLabel.isVisible()){
            // Vahvistetaan laina tässä
            int cardNumber = Integer.parseInt(cardNumberTextField.getText());
            Book selectedBook = (Book) booksTableView.getSelectionModel().getSelectedItem();
            
            selectedBook.decrementUnitsAvailable();
            unitsAvailableLabel.setText("Niteitä vapaana: "+selectedBook.getUnitsAvailable());
            
            loanButton.setText("Lainaa");
            cardNumberTextField.clear();
            
            // Tässä kutsutaan datan ja ohjainten välisestä viestinnästä vastaavaa ohjainta ja merkitään laina            
            this.dataController.setBookLoanToCustomer(selectedBook, cardNumber);
            
            // Tallenetaan muutokset myös BooksDataAccessObjectiin, jotta saadaan samat tiedot ensikerrallakin kun sovellus käynnistyy
            this.dataController.updateBooksPersistentStore();
            
            cardNumberTextField.setVisible(false);
            cancelLoanButton.setVisible(false);
            addCardNumberLabel.setVisible(false);
            
            System.out.println("Books: Loaned "+selectedBook.getName()+" for a customer with the card number "+cardNumber);
        }else{
            // Jos tekstikenttä kirjastokortin numerolle ei ole näkyvissä aloitetaan lainaus
            // Tässä tapahtuu lähinnä UI-muutoksia, joita varten olisi tarvittu oma nappi ilman tätä ehtolausetta
            cardNumberTextField.setVisible(true);
            cancelLoanButton.setVisible(true);
            addCardNumberLabel.setVisible(true);
            loanButton.setText("Vahvista laina");
        }  
    }
    
    @FXML 
    private void handleCancelLoanBookButtonAction(ActionEvent event){
        // Perutaan lainaus toimen aloitus ja palataan normaali UI:hin
        addCardNumberLabel.setVisible(false);
        cardNumberTextField.setVisible(false);
        cardNumberTextField.clear();
        cancelLoanButton.setVisible(false);
        loanButton.setText("Lainaa");
    }
    
    private void intializeBooksTableView(){
        // Tässä alustetaan taulukkonäkymä datalla, tätä kutsutaan tyypillisesti vain kerran kun ohjainta ollaan alustamassa
        //Book fields: name, creator, yearPublished, ISBN, units
        
        TableColumn nameColumn = new TableColumn("Nimi");
        nameColumn.setCellValueFactory(new PropertyValueFactory<Book, String>("name"));
        
        TableColumn creatorColumn = new TableColumn("Tekijä");
        creatorColumn.setCellValueFactory(new PropertyValueFactory<Book, String>("creator"));
    
        booksTableView.setItems(this.dataController.getBooks());
        booksTableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        booksTableView.getColumns().addAll(nameColumn, creatorColumn);
    }
        
    private void observeBookTableView(){
        // Tämä tarkkailee muutoksia taulukkonäkymässä ja muuttaa otiskkokenttiä valintojen perusteella
        booksTableView.getSelectionModel().selectedItemProperty().addListener(
            new ChangeListener<Book>() {
                public void changed(ObservableValue<? extends Book> ov, Book oldSelection, Book newSelectionl) {
                    
                    if(!isInAdditionMode){
                        System.out.println("Books: Selected book "+newSelectionl.getName());
                        
                        nameLabel.setText(newSelectionl.getName());
                        creatorLabel.setText("Tekijä: "+newSelectionl.getCreator());
                        ISBNLabel.setText("ISBN: "+newSelectionl.getISBN());
                        yearPublishedLabel.setText("Julkaisuvuosi: "+newSelectionl.getYearPublished());
                        unitsLabel.setText("Niteitä: "+newSelectionl.getUnits());
                        unitsAvailableLabel.setText("Niteitä vapaana: "+newSelectionl.getUnitsAvailable());
                    }
            }
        });
    }
}