/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package kirjastox.controller;

import kirjastox.model.Customer;
import kirjastox.model.Book;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import kirjastox.model.AudioRecord;
import kirjastox.model.CustomersDataAccessObject;

/**
 * FXML Controller class
 *
 * @author hki
 */
public class FXMLCustomerTabController implements Initializable {

    private ObservableList<Customer> customers;
    private ObservableList<Book> customerLoans;
    DataSingletonController dataController;
    
    private int nextCardNumber;
    private boolean isInAdditionMode;
   
    @FXML
    private TableView customersTableView;
    @FXML
    private TableView loansTableView;
    @FXML
    private Label nameLabel;
    @FXML
    private Label addNameLabel;
    @FXML
    private Label addressLabel;
    @FXML
    private Label cardNumberLabel;
    @FXML
    private Label finesLabel;
    @FXML
    private Label loanBanLabel;
    @FXML 
    private TextField nameTextField;  
    @FXML
    private TextField addressTextField; 
    @FXML
    private Button addCustomerButton;
    @FXML
    private Button removeCustomer;
    @FXML
    private Button saveCustomerButton;
    @FXML
    private Button cancelAddCustomerButton;
    @FXML
    private Button payFinesButton;
    @FXML
    private Button returnLoansButton;
    // Lisäys TextFieldit puuttuvat täältä ja FXML:stä
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.dataController = DataSingletonController.getInstance();
        
        intializeCustomerLoansTableView();
        intializeCustomersTableView();
        observeCustomersTableView();
        customersTableView.getSelectionModel().select(0);
        
        
    }
    
   @FXML 
    private void handleReturnLoansButtonAction(ActionEvent event){
        System.out.println("Customers: Return all loans");
        try {
            // Otetaan ensin valittu asiakas
            Customer selectedCustomer = (Customer) customersTableView.getSelectionModel().getSelectedItem();
            
            // Käydään valitun asiakkaan kirjalainat yksitellen läpi
            for(Book loanedBook: selectedCustomer.getBookLoans()){
                
                //Merkitään kirjan vapainaolevien niteiden määrä kasvaneeksi yhdellä
                this.dataController.setBookReturned(loanedBook);
            }
            
            // Pyyhitään asiakkaan kirjalainat
            selectedCustomer.getBookLoans().clear();
            
            // Tallennetaan tila vielä lopuksi tietovastoon
            this.dataController.updateCustomersPersistentStore();
        } catch (Exception e) {
            System.out.println("Customers: Returning customers loans failed");
        }
    }
    
    @FXML 
    private void handlePayFinesButtonAction(ActionEvent event){
        System.out.println("Customers: Pay all fines");
        
        try {
            Customer selectedCustomer = (Customer) customersTableView.getSelectionModel().getSelectedItem();
            
            // Tarkastetaan onko asiakkaalla maksettavia sakkoja, jottei turhaan päivitetä persistenttiä dataa
            if(selectedCustomer.getFines() > 0){
                selectedCustomer.payFines();
                this.dataController.updateCustomersPersistentStore();
            }
            
        } catch (Exception e) {
            System.out.println("Customers: Returning customers loans failed");
        }
    }
    
    @FXML
    private void handleAddCustomerButtonAction(ActionEvent event){
        System.out.println("Customer: Add new client");
        this.isInAdditionMode = true;
        nameLabel.setVisible(false);
        addNameLabel.setVisible(true);
        nameTextField.setVisible(true);
        addressLabel.setVisible(true);
        addressLabel.setText("Osoite: ");
        addressTextField.setVisible(true);
        
        //automaattinen kortin numeron generointi
        cardNumberLabel.setText("Kirjastokortti: "+this.nextCardNumber);
        cardNumberLabel.setVisible(true);
        finesLabel.setVisible(false);
        loanBanLabel.setVisible(false);
        saveCustomerButton.setVisible(true);
        cancelAddCustomerButton.setVisible(true);
        loansTableView.setVisible(false);
        payFinesButton.setVisible(false);
        returnLoansButton.setVisible(false);          
    }

    //Mahdollisesti jotain varmistusta?
    @FXML
    private void handleRemoveCustomerButtonAction(ActionEvent event){
        System.out.println("Customers: Remove an existing customer");
        try {
            Customer selectedCustomer = (Customer) customersTableView.getSelectionModel().getSelectedItem();
            this.dataController.getCustomers().remove(selectedCustomer);
            this.dataController.updateCustomersPersistentStore();
        } catch (Exception e) {
            System.out.println("Customers: Removing an existing customer failed");
        }
    }
    
    //uuden asiakkaan lisäämisen peruuttaminen
    @FXML
    private void handleCancelAddCustomerButtonAction(ActionEvent event){
        System.out.println("Customers: Adding a new customer cancelled");
        nameLabel.setVisible(true);
        nameTextField.setVisible(false);
        addressTextField.setVisible(false);
        addNameLabel.setVisible(false);
        cardNumberLabel.setVisible(true);
        finesLabel.setVisible(true);
        loanBanLabel.setVisible(true);
        saveCustomerButton.setVisible(false);
        cancelAddCustomerButton.setVisible(false);
        loansTableView.setVisible(true);
        payFinesButton.setVisible(true);
        returnLoansButton.setVisible(true); 
        
        this.isInAdditionMode = false;
        customersTableView.getSelectionModel().selectFirst();
    }

    @FXML 
    private void handleSaveCustomerButtonAction(ActionEvent event){
        System.out.println("Customers: Saving new customer");
        
        //name, creator, yearPublished, ISBN, units
        String name = nameTextField.getText();
        String address = addressTextField.getText();
  
        try{
            nameLabel.setVisible(true);
            nameTextField.setVisible(false);
            addressLabel.setVisible(true);
            addressTextField.setVisible(false);
            addNameLabel.setVisible(false);
            //automaattinen kortin numeron generointi
            cardNumberLabel.setVisible(true);
            finesLabel.setVisible(true);
            loanBanLabel.setVisible(true);
            saveCustomerButton.setVisible(false);
            cancelAddCustomerButton.setVisible(false);
            loansTableView.setVisible(true);
            payFinesButton.setVisible(true);
            returnLoansButton.setVisible(true);         

            
            Customer newCustomer = new Customer(name, address, nextCardNumber);
            this.dataController.getCustomers().add(newCustomer);
            nextCardNumber++;
            this.dataController.updateCustomersPersistentStore();
            
            
            this.isInAdditionMode = false;
            customersTableView.getSelectionModel().selectLast();
            
            

        }catch(Exception e){
            System.out.println("Customers: Saving a new customer failed");
        }
    }
    
    private void intializeCustomersTableView(){
    
    // Asiakas-taulukkonäkymän alustus 
    // Customer-mallin kentät: String name, String address, int cardNumber
        
    // Hieman tökerö ratkaisu, mutta toimii vielä tässä vaiheessa. Paremman saa tehdä :)
    nextCardNumber = this.dataController.getCustomers().size()+1;
    
    
    TableColumn nameColumn = new TableColumn("Nimi");
    nameColumn.setCellValueFactory(new PropertyValueFactory<Customer, String>("name"));
    
    TableColumn cardColumn = new TableColumn("Kortti");
    cardColumn.setCellValueFactory(new PropertyValueFactory<Customer, Integer>("cardNumber"));

    customersTableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
    customersTableView.setItems(this.dataController.getCustomers());
    customersTableView.getColumns().addAll(nameColumn, cardColumn);   
    }
    
    private void intializeCustomerLoansTableView(){
        
        TableColumn nameColumn = new TableColumn("Kirjan nimi");
        nameColumn.setCellValueFactory(new PropertyValueFactory<Book, String>("name"));
    
        TableColumn creatorColumn = new TableColumn("Tekijä");
        creatorColumn.setCellValueFactory(new PropertyValueFactory<Book, Integer>("creator"));

        loansTableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        loansTableView.getColumns().addAll(nameColumn, creatorColumn);
    }
    
    
    private void observeCustomersTableView(){
        // Tämä metodi käynnistää asiakastaulunäkymän muutosten kuuntelun
        
        customersTableView.getSelectionModel().selectedItemProperty().addListener(
            new ChangeListener<Customer>() {
                public void changed(ObservableValue<? extends Customer> ov, Customer oldSelection, Customer newSelection) {
                    
                    if(!isInAdditionMode){
                    System.out.println("Asiakas: "+newSelection.getName()+" valittu");
                    
                        
                    nameLabel.setText(newSelection.getName());
                    addressLabel.setText("Osoite: "+newSelection.getAddress());
                    cardNumberLabel.setText("Kirjastokortti: "+newSelection.getCardNumber());
                    finesLabel.setText("Sakot (EUR): "+newSelection.getFines());
                        
                    if(newSelection.hasLoanBan()){
                        loanBanLabel.setText("Asiakas on lainauskiellossa!");
                    }
                    
                    customerLoans = FXCollections.observableArrayList(newSelection.getBookLoans());
                    loansTableView.setItems(customerLoans);
                    
                   }
            }
        });
    
    }
    
    private void observeCustomerLoansTableView(){
        /*
        Katso mallia BooksTabControllerista!
        */   
    }
    


}
